package br.org.centeleghe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProfessorRepositorio {
     public void cadastrar(Professor professor){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.cadastrar(professor);
        
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into professorOO "
                + "(id_pessoa, salario_professor, disciplina_professor) values (?, ?, ?)";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, professor.getId());
            preparedStatement.setDouble(2, professor.getSalario());
            preparedStatement.setString(3, professor.getDisciplina());
            
            preparedStatement.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
     
    public void delete(Professor professor){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String deleteSQL = "delete from professorOO where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, professor.getId());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
         pessoaRepositorio.delete(professor);
    }
    public ArrayList getAll(){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        
        Statement statement;
        
        String selectSQL = "select * from professorOO";
        try{
            statement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                Professor professor = new Professor();
                professor.setId(rs.getInt("id_pessoa"));
                pessoaRepositorio.getOne(professor);
                professor.setSalario(rs.getDouble("salario_professor"));
                professor.setDisciplina(rs.getString("disciplina_professor"));
                lista.add(professor);
            }
                
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void update(Professor professor) {
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.update(professor);
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        
        String insertTableSQL = "update professorOO " + "set salario_professor = ?, " 
                + "disciplina_professor = ? where id_pessoa = ?";
         
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            
            ps.setDouble(1, professor.getSalario());
            ps.setString(2, professor.getDisciplina());
            ps.setInt(3, professor.getId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
