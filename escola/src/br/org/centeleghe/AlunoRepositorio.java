package br.org.centeleghe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AlunoRepositorio {
    public void cadastrar(Aluno aluno){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.cadastrar(aluno);
        
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into alunoOO "
                + "(id_pessoa, semestre_aluno, curso_aluno) values (?, ?, ?)";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, aluno.getId());
            preparedStatement.setString(2, aluno.getSemestre());
            preparedStatement.setString(3, aluno.getCurso());
            
            preparedStatement.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void delete(Aluno aluno){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String deleteSQL = "delete from alunoOO where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, aluno.getId());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        pessoaRepositorio.delete(aluno);
    }
    
    public ArrayList getAll(){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        
        Statement statement;
        
        String selectSQL = "select * from alunoOO";
        try{
            statement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                Aluno aluno = new Aluno();
                aluno.setId(rs.getInt("id_pessoa"));
                pessoaRepositorio.getOne(aluno);
                aluno.setSemestre(rs.getString("semestre_aluno"));
                aluno.setCurso(rs.getString("curso_aluno"));
                lista.add(aluno);
            }
                
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
        public void update(Aluno aluno) {
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.update(aluno);
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        
        String insertTableSQL = "update alunoOO " + "set semestre_aluno = ?, " 
                + "curso_aluno = ? where id_pessoa = ?";
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            
            ps.setString(1, aluno.getSemestre());
            ps.setString(2, aluno.getCurso());
            ps.setInt(3, aluno.getId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
