package br.org.centeleghe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FuncaoAdministrativaRepositorio {
    public void cadastrar(FuncaoAdministrativa adm){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.cadastrar(adm);
        
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into admOO "
                + "(id_pessoa, salario_adm, setor_adm, funcao_adm) values (?, ?, ?, ?)";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, adm.getId());
            preparedStatement.setDouble(2, adm.getSalario());
            preparedStatement.setString(3, adm.getFuncao());
            preparedStatement.setString(4, adm.getSetor());
            
            preparedStatement.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void delete(FuncaoAdministrativa adm){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String deleteSQL = "delete from admOO where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, adm.getId());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        pessoaRepositorio.delete(adm);
    }
    
    public ArrayList getAll(){
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        
        Statement statement;
        
        String selectSQL = "select * from admOO";
        try{
            statement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                FuncaoAdministrativa adm = new FuncaoAdministrativa();
                adm.setId(rs.getInt("id_pessoa"));
                pessoaRepositorio.getOne(adm);
                adm.setSalario(rs.getDouble("salario_adm"));
                adm.setSetor(rs.getString("setor_adm"));
                adm.setFuncao(rs.getString("funcao_adm"));
                lista.add(adm);
            }
                
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
     public void update(FuncaoAdministrativa adm) {
        PessoaRepositorio pessoaRepositorio = new PessoaRepositorio();
        pessoaRepositorio.update(adm);
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        
        String insertTableSQL = "update admOO " + "set salario_adm = ?, " 
                + "setor_adm = ?, funcao_adm = ? where id_pessoa = ?";
         
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            
            ps.setDouble(1, adm.getSalario());
            ps.setString(2, adm.getSetor());
            ps.setString(3, adm.getFuncao());
            ps.setInt(4, adm.getId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
