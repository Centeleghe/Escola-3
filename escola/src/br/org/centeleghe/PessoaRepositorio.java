package br.org.centeleghe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PessoaRepositorio {
    public void cadastrar(Pessoa pessoa){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into pessoaOO "
                + "(id_pessoa, tipo_pessoa, nome_pessoa, idade_pessoa, "
                + "endereco_pessoa) values "
                +"(?, ?, ?, ?, ?)";
        
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, pessoa.getId());
            preparedStatement.setString(2, pessoa.getTipo());
            preparedStatement.setString(3, pessoa.getNome());
            preparedStatement.setInt(4, pessoa.getIdade());
            preparedStatement.setString(5, pessoa.getEndereco());
            
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void delete(Pessoa pessoa){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String deleteSQL = "delete from PessoaOO where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, pessoa.getId());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void getOne(Pessoa pessoa){
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        
        String selectSQL = "SELECT * FROM pessoaOO WHERE id_pessoa = ?";
        
        PreparedStatement preparedStatement;
        
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, pessoa.getId());
            
            ResultSet resultado = preparedStatement.executeQuery();
            
            if(resultado.next()){
                pessoa.setId(resultado.getInt("id_pessoa"));
                pessoa.setNome(resultado.getString("nome_pessoa"));
                pessoa.setEndereco(resultado.getString("endereco_pessoa"));
                pessoa.setTipo(resultado.getString("tipo_pessoa"));
                pessoa.setIdade(resultado.getInt("idade_pessoa"));
            }
                    
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            conexao.desconecta();
        }
    }
    
    public void update(Pessoa pessoa) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        
        String insertTableSQL = "update pessoaOO " + "set nome_pessoa = ?, " 
                + "endereco_pessoa = ?, " + " idade_pessoa = ?, "
                +"tipo_pessoa = ? where id_pessoa = ?";
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getEndereco());
            ps.setInt(3, pessoa.getIdade());
            ps.setString(4, pessoa.getTipo());
            ps.setInt(5, pessoa.getId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
